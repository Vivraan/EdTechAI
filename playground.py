import pandas as pd
import re
from bs4 import BeautifulSoup

df = pd.read_csv('test_questions_type_and_options.csv',
                 na_filter=False, engine='python')
text = '\n'.join(df['QuestionContent'].array)
text += '\n'.join(df['AnswerOptions'].array)
text = text.replace('[', '').replace(']', '').replace('"', '')
text = re.sub(r'\\u(\d+)', lambda match: chr(int(match.group(1), 16)), text)
text = BeautifulSoup(text, 'lxml').text

with open('questions_text.txt', 'w', encoding='utf-8') as out:
    out.write(text)
